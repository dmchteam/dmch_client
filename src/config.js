
export const getHeader = function () {
  const tokenData = JSON.parse(window.localStorage.getItem('auth_user'))
  console.log(tokenData)
  const header = {
    'Accept': 'application/json',
    'Authorization': 'Bearer ' + tokenData.access_token
  }

  return header
}
