import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Coba from '@/components/Coba'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/coba',
      name: 'Coba',
      component: Coba
    }
  ]
})
